/* goapplication.go */

package main

import (
	"log"
	"fmt"
	"os"
	"path/filepath"
	
	win "gitlab.com/win32go/win32"
)

var desktop *win.GoDeskTopWindow
var mainwindow1 *win.GoWindowView
var mainwindow2 *win.GoWindowView

func main() {
	appName := filepath.Base(os.Args[0]) 	// GoApplication.exe
	appName = appName[:len(appName) - 4]	// GoApplication

	app := win.GoApplication(appName)

	desktop = app.GoDeskTop()
	//screenCX := desktop.ClientHeight()
	//screenCY := desktop.ClientWidth()

	mainwindow1 = win.GoWindow(nil)
	mainwindow1.SetTitle("GoWindows Demo #1")
	mainwindow1.SetMargin(40)
	mainwindow1.SetOnPaint(MainWindow1_Paint)
	mainwindow1.Show()

	mainwindow2 = win.GoWindow(nil)
	mainwindow2.SetTitle("GoWindows Demo #2")
	mainwindow2.SetMargin(40)
	mainwindow2.SetOnPaint(MainWindow2_Paint)
	mainwindow2.Show()
		
	ret := app.Run()
	fmt.Println("Window Closed return =", ret)
}

func MainWindow1_Paint(p *win.GoPainter) {
	log.Println("MainWindow1_Paint..............")
	blue := win.CreateRGBColor(0, 0, 255)
	log.Println("CreatePen(blue):")	
	pen := win.CreatePen(blue, 3, 1, "blue5")
	p.SetPen(pen)
	p.DrawRect(p.X()-10, p.Y()-10, p.Width()+20, p.Height()+20)

}

func MainWindow2_Paint(p *win.GoPainter) {
	log.Println("MainWindow2_Paint..............")
	red := win.CreateRGBColor(255, 0, 0)
	log.Println("SetStockPenColor(red):")
	p.SetStockPenColor(red)
	p.DrawRect(p.X()-10, p.Y()-10, p.Width() + 20, p.Height() + 20)
}